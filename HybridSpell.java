public class HybridSpell extends SpellDecorator {
    public HybridSpell(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getMagicDamage() {
        return (int) Math.ceil((float) decoratedSpell.calculateDamage() / 2);
    }

    @Override
    public int getPhysicalDamage() {
        return getMagicDamage();
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 1;
    }
}
