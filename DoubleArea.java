public class DoubleArea extends SpellDecorator {
    public DoubleArea(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getAreaOfEffect() {
        return decoratedSpell.getAreaOfEffect() * 2;
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 5;
    }
}
