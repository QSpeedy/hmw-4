public class StoneThrow extends Spell {
    public StoneThrow() {
        physicalDamage = 5;
        range = 3;
        areaOfEffect = 1;
        mpCost = 2;
    }
}
