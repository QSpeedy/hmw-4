public class DoubleDamage extends SpellDecorator {
    public DoubleDamage(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int calculateDamage() {
        return decoratedSpell.calculateDamage() * 2;
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 10;
    }
}
