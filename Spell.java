public abstract class Spell {
    protected int magicDamage;
    protected int physicalDamage;
    protected int fireDamage;
    protected int electricDamage;
    protected int coldDamage;
    protected int acidDamage;
    protected int sonicDamage;
    protected int range;
    protected int areaOfEffect;
    protected int mpCost;

    public int getMagicDamage() {
        return magicDamage;
    }

    public int getPhysicalDamage() {
        return physicalDamage;
    }

    public int getFireDamage() {
        return fireDamage;
    }

    public int getElectricDamage() {
        return electricDamage;
    }

    public int getColdDamage() {
        return coldDamage;
    }

    public int getAcidDamage() {
        return acidDamage;
    }

    public int getSonicDamage() {
        return sonicDamage;
    }

    public int getRange() {
        return range;
    }

    public int getAreaOfEffect() {
        return areaOfEffect;
    }

    public int getMpCost() {
        return mpCost;
    }

    public int calculateDamage() {
        return magicDamage + physicalDamage + fireDamage + electricDamage + coldDamage + acidDamage + sonicDamage;
    }
}
