public class DoubleRange extends SpellDecorator {
    public DoubleRange(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getRange() {
        return decoratedSpell.getRange() * 2;
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 3;
    }
}
