public class MagicMissile extends Spell {
    public MagicMissile() {
        magicDamage = 5;
        range = 10;
        areaOfEffect = 1;
        mpCost = 11;
    }
}
