public class AcidArrow extends Spell {
    public AcidArrow() {
        acidDamage = 5;
        range = 7;
        areaOfEffect = 1;
        mpCost = 9;
    }
}

