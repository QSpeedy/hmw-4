public class Minimize extends SpellDecorator {
    public Minimize(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getMagicDamage() {
        return Math.max(decoratedSpell.getMagicDamage() / 2, 0);
    }


    @Override
    public int getMpCost() {
        return Math.max(decoratedSpell.getMpCost() / 2, 0);
    }
}
