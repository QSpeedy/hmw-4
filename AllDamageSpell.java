public class AllDamageSpell extends SpellDecorator {
    public AllDamageSpell(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getMagicDamage() {
        return (int) Math.ceil((float) decoratedSpell.calculateDamage() / 7);
    }

    @Override
    public int getPhysicalDamage() {
        return getMagicDamage();
    }

    @Override
    public int getFireDamage() {
        return getMagicDamage();
    }

    @Override
    public int getElectricDamage() {
        return getMagicDamage();
    }

    @Override
    public int getColdDamage() {
        return getMagicDamage();
    }

    @Override
    public int getAcidDamage() {
        return getMagicDamage();
    }

    @Override
    public int getSonicDamage() {
        return getMagicDamage();
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 7;
    }
}
