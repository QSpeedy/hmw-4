public class Maximize extends SpellDecorator {
    public Maximize(Spell decoratedSpell) {
        super(decoratedSpell);
    }

 
    @Override
    public int getMagicDamage() {
        return decoratedSpell.getMagicDamage() * 2;
    }

  

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() * 2;
    }
}
