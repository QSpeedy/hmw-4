public class LightningBolt extends Spell {
    public LightningBolt() {
        electricDamage = 5;
        range = 5;
        areaOfEffect = 1;
        mpCost = 7;
    }
}
