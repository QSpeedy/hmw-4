public class FourElementSpell extends SpellDecorator {
    public FourElementSpell(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int getFireDamage() {
        return (int) Math.ceil((float) decoratedSpell.calculateDamage() / 4);
    }

    @Override
    public int getColdDamage() {
        return getFireDamage();
    }

    @Override
    public int getElectricDamage() {
        return getFireDamage();
    }

    @Override
    public int getAcidDamage() {
        return getFireDamage();
    }

    @Override
    public int getMpCost() {
        return decoratedSpell.getMpCost() + 2;
    }
}
