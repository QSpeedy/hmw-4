public class Fireball extends Spell {
    public Fireball() {
        fireDamage = 5;
        range = 3;
        areaOfEffect = 3;
        mpCost = 10;
    }
}
