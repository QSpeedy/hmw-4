public class FireSpell extends SpellDecorator {
    public FireSpell(Spell decoratedSpell) {
        super(decoratedSpell);
    }

    @Override
    public int calculateDamage() {
        return decoratedSpell.calculateDamage();
    }

    @Override
    public int getFireDamage() {
        return calculateDamage();
    }

   
}
