import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SpellTest {

   
    @Test
    public void testFireballProperties() {
        Spell fireball = new Fireball();
        assertEquals(5, fireball.getFireDamage());
        assertEquals(3, fireball.getRange());
        assertEquals(3, fireball.getAreaOfEffect());
        assertEquals(10, fireball.getMpCost());
    }

    @Test
    public void testLightningBoltProperties() {
        Spell lightningBolt = new LightningBolt();
        assertEquals(5, lightningBolt.getElectricDamage());
        assertEquals(5, lightningBolt.getRange());
        assertEquals(1, lightningBolt.getAreaOfEffect());
        assertEquals(7, lightningBolt.getMpCost());
    }

    @Test
    public void testConeOfColdProperties() {
        Spell coneOfCold = new ConeOfCold();
        assertEquals(5, coneOfCold.getColdDamage());
        assertEquals(5, coneOfCold.getRange());
        assertEquals(2, coneOfCold.getAreaOfEffect());
        assertEquals(13, coneOfCold.getMpCost());
    }

    @Test
    public void testDoubleDamageDecorator() {
        Spell fireball = new Fireball();
        Spell doubleDamageFireball = new DoubleDamage(fireball);
        assertEquals(10, doubleDamageFireball.calculateDamage());
        assertEquals(20, doubleDamageFireball.getMpCost());
    }

    @Test
    public void testDoubleRangeDecorator() {
        Spell lightningBolt = new LightningBolt();
        Spell doubleRangeLightningBolt = new DoubleRange(lightningBolt);
        assertEquals(10, doubleRangeLightningBolt.getRange());
        assertEquals(10, doubleRangeLightningBolt.getMpCost());
    }


    @Test
    public void testDoubleDamageDoubleRange() {
        Spell lightningBolt = new LightningBolt();
        Spell modifiedSpell = new DoubleDamage(new DoubleRange(lightningBolt));
        assertEquals(10, modifiedSpell.calculateDamage());
        assertEquals(10, modifiedSpell.getRange());
        assertEquals(20, modifiedSpell.getMpCost());
    }

    @Test
    public void testMaximizeMinimizeCombination() {
        Spell coneOfCold = new ConeOfCold();
        Spell modifiedSpell = new Maximize(new Minimize(coneOfCold));
        assertEquals(5, modifiedSpell.getColdDamage()); 
        assertEquals(13, modifiedSpell.getMpCost()); 
    }


@Test
public void testAcidArrowProperties() {
    Spell acidArrow = new AcidArrow();
    assertEquals(5, acidArrow.getAcidDamage());
    assertEquals(7, acidArrow.getRange());
    assertEquals(1, acidArrow.getAreaOfEffect());
    assertEquals(9, acidArrow.getMpCost());
}

@Test
public void testScreechProperties() {
    Spell screech = new Screech();
    assertEquals(5, screech.getSonicDamage());
    assertEquals(5, screech.getRange());
    assertEquals(5, screech.getAreaOfEffect());
    assertEquals(18, screech.getMpCost());
}

@Test
public void testStoneThrowProperties() {
    Spell stoneThrow = new StoneThrow();
    assertEquals(5, stoneThrow.getPhysicalDamage());
    assertEquals(3, stoneThrow.getRange());
    assertEquals(1, stoneThrow.getAreaOfEffect());
    assertEquals(2, stoneThrow.getMpCost());
}

@Test
public void testMagicMissileProperties() {
    Spell magicMissile = new MagicMissile();
    assertEquals(5, magicMissile.getMagicDamage());
    assertEquals(10, magicMissile.getRange());
    assertEquals(1, magicMissile.getAreaOfEffect());
    assertEquals(11, magicMissile.getMpCost());
}

@Test
public void testDoubleAreaDecorator() {
    Spell coneOfCold = new ConeOfCold();
    Spell doubleAreaConeOfCold = new DoubleArea(coneOfCold);
    assertEquals(4, doubleAreaConeOfCold.getAreaOfEffect());
    assertEquals(18, doubleAreaConeOfCold.getMpCost());
}

@Test
public void testMinimizeDecorator() {
    Spell fireball = new Fireball();
    Spell minimizedFireball = new Minimize(fireball);
    assertEquals(2, minimizedFireball.getFireDamage());
    assertEquals(5, minimizedFireball.getMpCost());
}

@Test
public void testFireSpellDecorator() {
    Spell magicMissile = new MagicMissile();
    Spell fireMagicMissile = new FireSpell(magicMissile);
    assertEquals(5, fireMagicMissile.getFireDamage());
    assertEquals(0, fireMagicMissile.getMagicDamage());
}


@Test
public void testComplexDecoratorCombination() {
    Spell screech = new Screech();
    Spell complexSpell = new Maximize(new DoubleRange(new FireSpell(screech)));
    assertEquals(10, complexSpell.getRange());
    assertEquals(10, complexSpell.getFireDamage());
    assertEquals(36, complexSpell.getMpCost());
}
}
