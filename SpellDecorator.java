public abstract class SpellDecorator extends Spell {
    protected Spell decoratedSpell;

    public SpellDecorator(Spell decoratedSpell) {
        this.decoratedSpell = decoratedSpell;
    }
}
