public class ConeOfCold extends Spell {
    public ConeOfCold() {
        coldDamage = 5;
        range = 5;
        areaOfEffect = 2;
        mpCost = 13;
    }
}
