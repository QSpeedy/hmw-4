public class Screech extends Spell {
    public Screech() {
        sonicDamage = 5;
        range = 5;
        areaOfEffect = 5;
        mpCost = 18;
    }
}
